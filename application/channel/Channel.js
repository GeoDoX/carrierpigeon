const Server = require("./../server/Server");

module.exports = class Channel
{
    constructor()
    {
        this.controller = null;
    }

    send(message)
    {
        // Server.send(message);
    }

    receive(message)
    {
        if (this.controller)
            this.controller.receive(message);
    }
}