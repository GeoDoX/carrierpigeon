const {remote} = require("electron");
const {ipcRenderer} = require("electron");

let windowHandle = remote.getCurrentWindow();
let windowMaximized = windowHandle.isMaximized();

requestTitle();
applyTitleBarControls();
applyRoundedCorners();
alignTitleBarControls();
fixMaximizedDrag();

function requestTitle()
{
    ipcRenderer.send("titlebar-title");
    ipcRenderer.on("titlebar-title-retrieved", (event, data) => applyTitle(data.title));
}

function applyTitle(title)
{
    let titlebarTitle = $("#titlebar-title")[0];
    titlebarTitle.innerHTML = title;
}

function applyTitleBarControls()
{
    $("#titlebar-minimize").on("click", () => windowHandle.minimize());
    $("#titlebar-maximize").on('click', () => windowToggleMaximize());
    $("#titlebar-close").on('click', () => windowHandle.close());
}

function windowToggleMaximize()
{
    windowMaximized ? windowHandle.unmaximize() : windowHandle.maximize();
    windowMaximized = !windowMaximized;

    // If we should toggle corners (eventually)
    toggleRoundedCorners();
}

function toggleRoundedCorners()
{
    windowMaximized ? unapplyRoundedCorners() : applyRoundedCorners();
}

function applyRoundedCorners()
{
    $("#frame")[0].classList.add("frame-round");
    $("#titlebar")[0].classList.add("titlebar-round");
}

function unapplyRoundedCorners()
{
    $("#frame")[0].classList.remove("frame-round");
    $("#titlebar")[0].classList.remove("titlebar-round");
}

function alignTitleBarControls()
{
    let titlebarControls = $("#titlebar-controls").val();

    if (titlebarControls && titlebarControls.classList.contains("align-left"))
    {
        for (let child of titlebarControls.childNodes)
            titlebarControls.insertBefore(child, titlebarControls.firstChild);
    }
}

function fixMaximizedDrag()
{
    $("#titlebar")[0].addEventListener("drag", (event) =>
    {
        if (windowMaximized)
            windowToggleMaximize();
    });
}
